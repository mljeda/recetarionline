<!DOCTYPE html>
<html>
<head>
	<title>Modificar Recetas</title>
	<link rel ="stylesheet" href="css/estilossubir.css">
</head>


</head>
<body>
     <?php
     include ("conexion.php");
     $id = $_REQUEST['id'];
     $query = "SELECT * FROM recetario WHERE id = '$id'";
     $resultado =$conexion->query($query);
     $row =$resultado->fetch_assoc();
     ?>

	<form action="proceso_modificar.php?id=<?php echo $row['nombre']; ?>" method="post" enctype="multipart/form-data">

        
		<p><label>Nombre de la receta</label>
		<input type="text" name="nombre" value="<?php echo $row['nombre'];  ?>"></p>

		<p>
		<label>Imagen de la receta</label>
		<img src="data:image/jpg;base64,<?php echo base64_encode($row['imagen']); ?>"/>
		<input type="file" name="imagen" placeholder="cargar">
	    </p>

        <p>
		<label>Ingredientes</label>
		<input  type="text" name="ingredientes" value="<?php echo $row['ingredientes'] ?>">	    
	  </p>
        
        <p>
		<label>Procedimiento</label>
		<input class="imagen" type="text" name="procedimiento" value="<?php echo $row['procedimiento'];  ?>">
	   </p>

	   <p>
		<label>Categoria</label>
		<input list ="categoria" name="categoria" >
		<datalist id="categoria" >
		<option value= "postres"> 
		<option value="bebidas">
		<option value="ensaladas">
		</datalist>
	    </p>

	   <p>
	   	<button type="submit" >Enviar Receta</button>
	   </p>

	   <a href="php/bandera.php?id=<?php echo $row['id']?>"><button>Regresar</button></a> 
	</form>

</body>
</html>