<?php
function crea_fichero($cadena)
{
    $flujo = fopen('recetas.xml', 'w');//creamos el fichero.
    fputs($flujo, $cadena);//volcamos el contenido de cadena al fichero
    fclose($flujo);//cerramos el flujo
    $ruta="recetas.xml";
    header("Content-Disposition:attachment; filename=".$ruta);
    header("Content-Type: application/octet-stream");
    header("Content-Length: ".filesize($ruta));
    readfile($ruta);
}

$opc = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
$dsn = "mysql:host=localhost;dbname=dbprueba";//datos conexion baseusuario
$usuario = 'root';
$contrasena = '';

$dwes = new PDO($dsn, $usuario, $contrasena, $opc);//conexion

$sql="SELECT * FROM recetario2";

if (isset($dwes))
{
    $resultado = $dwes->query($sql);//obtenemos las tablas de la base de datos
    $xml="<?xml version=\"1.0\"?>\n";//variable que contendra el codigo xml
    $xml .= "<informacion>\n";

    $row = $resultado->fetch();
    $xml .= "\t<".$row[0].">\n";//tabla de la base de datos
    $result= $dwes->query($sql);//obtenemos todos los campos de la  tabla
    while($fila = $result->fetch(PDO::FETCH_ASSOC))
    {
        $xml .= "\t\t<registro>\n";//por cada registro de la tabla

   	    foreach ($fila as $k => $v) 
   	    {//recorremos las claves y los valores de cada registro
            $xml .= "\t\t\t<".$k.">".$v."</".$k.">\n";//los almacenamos en el xml
        }
        $xml .= "\t\t</registro>\n";//cerramos el registro
    }
    $xml .= "\t</".$row[0].">\n";//cerramos la tabla
  
    $xml .="</informacion>";//cerramos el fichero xml
    //echo $xml;
    crea_fichero($xml);
    echo "<script>alert('Elemento Guardado');
    location.href='principal_normal.php';
    </script>";
}

?>