<!DOCTYPE html>
<html>
<head>
	<title>Ver Recetas</title>
	<link rel= "stylesheet" type ="text/css" href="css/csstablas.css">
	<link href="https://fonts.googleapis.com/css?family=Pacifico|Sacramento" rel="stylesheet">
</head>
</head>
<body>
	<div>
	<table >
		<thead>
			<tr >
			<th>Id</th>
			<th>Nombre</th>
			<th>Imagen</th>
			<th>Ingredientes </th>
			<th id="procedimiento">Procedimiento </th>
			<th id="ingredientes">Categoria </th>
			<th>Generar xml</th>
			<th colspan="3">Opiniones</th>
			

			<tr>  
		</thead>

		<tbody>
		<?php
			include("conexion.php");

			$query = "SELECT * FROM recetario";
			$resultado= $conexion->query($query);
			while($row = $resultado->fetch_assoc()){
		?>
			<tr>
				<td><?php echo $row['id']; ?></td>
				<td><?php echo $row['nombre'];?></td>
				<td><img height="70px" src="data:image/jpg;base64,<?php echo base64_encode($row['imagen']); ?>"/> </td>
				<td><?php echo $row['ingredientes']; ?></td>
				<td ><?php echo $row['procedimiento']; ?></td>
				<td><?php echo $row['categoria']; ?></td>
				<td><a href="xmlelemento.php?nombre=<?php echo $row['nombre']?>">Descargar</a></td>
				<td><a href="php/status1.php?id=<?php echo $row['id']?>">Modificar </a></td>
                <td><a href="calificar.php?nombre=<?php echo $row['nombre'] ?>&id=<?php echo $row['id'] ?> "> Calificar </a></td>
				<td><a href="comentar.php?nombre=<?php echo $row['nombre'] ?>&id=<?php echo $row['id'] ?> "> Comentar </a></td>
			</tr>


		<?php
           }

          ?>



		</tbody>
	</table>

 <a href="xmltabla.php"><button>Descargar recetas</button></a>
 <a href="principal_normal.php"><button>Regresar</button></a> 


</div>
</body>
</html>
