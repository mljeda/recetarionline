<!DOCTYPE html>
<html>
<head><?php 
include ("seguridad.php");
$sesion = $_SESSION['usuario'];

 ?>
	<title>Bienvenido</title>
	<link rel ="stylesheet" href="css/estilo_principal.css">
  <link href="https://fonts.googleapis.com/css?family=Pacifico|Sacramento|Indie Flower" rel="stylesheet">
</head>
<body>

  <div id ="agrupar" class="container">
    <H1> Bienvenido <?php echo $sesion ?></H1>

	     <nav id="menu"> 
          <ul>
            <li> <a href="postres.php"> Postres </a></li>
            <li> <a href="bebidas.php"> Bebidas </a></li>
            <li> <a href="ensaladas.php"> Ensaladas</a></li>
            <li> <a href="subir.html"> Subir Receta </a></li>
            <li> <a href="mostrar.php"> Ver recetas</a></li>
            <li> <a href="eliminaUsuario.php"> Eliminar Usuario</a></li>
            <li> <a href="index.php"> Salir</a></li>
          </ul>
        </nav>
          
        <section id="seccion">
          
        </section>
           
        <article id="arquitectura">
          <header>
              <h2>Mejor calificadas</h2>
          </header>
          <table>
            <thead>
              <tr >
                
                <th>Nombre</th>
                <th>Imagen</th>
                <th>Categoria</th>
              <tr>  
            </thead>
            <tbody>
              <?php
              include("conexion.php");
              $query = "SELECT * FROM recetario ORDER BY calificacion DESC";
              $resultado= $conexion->query($query);
              while($row = $resultado->fetch_assoc())
              {
                ?>
                <tr>                  
                  <td><?php echo $row['nombre'];?></td>
                  <td><img src="data:image/jpg;base64,<?php echo base64_encode($row['imagen']); ?>"/> </td>                  
                  <td><?php echo $row['categoria']; ?></td>
                </tr>
                <?php
              }
              ?>
            </tbody>
          </table>
        </article>
  </div>
</body>
</html>