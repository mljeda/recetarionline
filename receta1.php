<!DOCTYPE html>
<html>
<head>
	<title>Recetas</title>
	<link rel= "stylesheet" type ="text/css" href="css/csstablas.css">
	<link href="https://fonts.googleapis.com/css?family=Pacifico|Sacramento" rel="stylesheet">
		<?php
			include("conexion.php");

			$query = "SELECT * FROM recetario where id= 19";
			$resultado= $conexion->query($query);
			while($row = $resultado->fetch_assoc()){
		?>
</head>
<body>
	<div>
		<table>
			<tr>
				<th>
					Nombre
				</th>
				<td>
					<?php echo $row['nombre'];?>;
				</td>
			</tr>
			<tr>
				<th>
					Ingredientes
				</th>
				<td>
					<?php echo $row['ingredientes'];?>;
				</td>
			</tr>
			<tr>
				<th>
					Categoria
				</th>
				<td>
					<?php echo $row['categoria'];?>;
				</td>
			</tr>
			<tr>
				<th>
					Puntaje
				</th>
				<td>
					<?php echo $row['calificacion'];?>;
				</td>
			</tr>
		<?php
           }
        ?>
		</table>
	</div>
</body>
</html>

