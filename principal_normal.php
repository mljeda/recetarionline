<!DOCTYPE html>
<html>
<head>
<?php
include("conexion.php"); 
include "seguridad.php";
$sesion = $_SESSION['usuario'];
$online="INSERT INTO online(online) values('$sesion')";
$resultado1=$conexion->query($online);
$query1="SELECT DISTINCT online FROM online";
$resultado2=$conexion->query($query1);
?>
  <title>Bienvenido</title>
  <link rel ="stylesheet" href="css/estilo_principal.css">
    <link href="https://fonts.googleapis.com/css?family=Pacifico|Sacramento|Indie Flower" rel="stylesheet">
</head>
<body>
  <div id ="agrupar" class="container">
    <H1 > Bienvenido <?php echo $sesion ?></H1>
 

       <nav id="menu"> 
          <ul>
            <li> <a href="postres_normal.php"> Postres </a></li>
            <li> <a href="bebidas_normal.php"> Bebidas </a></li>
            <li> <a href="ensaladas_normal.php"> Ensaladas</a></li>
            <li> <a href="subir.html"> Subir Receta </a></li>
            <li> <a href="mostrar_normal.php"> Ver recetas</a></li>
            <li> <a href="php/salir.php"> Salir</a></li>
          </ul>
        </nav>

        <article id="arquitectura">
          <header>
              <h2>Las mas votadas</h2>
          </header>
          
          <table>
            <thead>
              <tr >
                
                <th>Nombre</th>
                <th>Imagen</th>
                <th>Categoria</th>
              <tr>  
            </thead>
            <tbody>
              <?php
              $query = "SELECT * FROM recetario ORDER BY calificacion DESC";
              $resultado= $conexion->query($query);
              while($row = $resultado->fetch_assoc())
              {
                ?>
                <tr>                  
                  <td><?php echo $row['nombre'];?></td>
                  <td ><img src="data:image/jpg;base64,<?php echo base64_encode($row['imagen']); ?>"/> </td>                  
                  <td><?php echo $row['categoria']; ?></td>
                </tr>
                <?php
              }
              ?>
            </tbody>
          </table>
        </article>
        <table>
          <thead>
            <tr>
              <th colspan="2">En linea</th>
            </tr>
          </thead>
          <tbody>
            <?php
            while($row1 = $resultado2->fetch_assoc())
            {               
            ?>
            <tr>
              <td><?php echo $row1['online'];?></td>
              <td><a href="" target="_blank"><img alt="Siguenos en Linkedin" src="https://lh5.googleusercontent.com/-8C0OdSp_7ZA/T3nN0G_313I/AAAAAAAAAsU/6_Hbu6Of3qU/s32/linkedin32.png" width=10 height=10  /></a>
              </td>
            </tr>
            <?php
            }
            ?>
          </tbody>
        </table>

    </div>

</body>
</html>