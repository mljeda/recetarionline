<!DOCTYPE html>

<html>
<head>
  <title>Ver Recetas</title>
  <link rel= "stylesheet" type ="text/css" href="css/csstablas.css">
  <link href="https://fonts.googleapis.com/css?family=Pacifico|Sacramento" rel="stylesheet">
</head>
<body>
  <table border="2">
    <thead>
      <tr >
      <th>Nombre</th>
      <th>Apellido P</th>
      <th>Apellido M </th>
      <th>Email </th>
      <th>Eliminar</th>
      <tr>  
    </thead>

    <tbody>
    <?php
      include("conexion.php");

      $query = "SELECT * FROM usuario where tipousuario='0'";
      $resultado= $conexion->query($query);
      while($row = $resultado->fetch_assoc()){
    ?>
      <tr>
        <td><?php echo $row['usuario'];?></td>
        <td><?php echo $row['apaterno']; ?></td>
        <td><?php echo $row['amaterno']; ?></td>
        <td><?php echo $row['email']; ?></td>
        <td><a href="php/eliminarUsuario.php?usuario=<?php echo $row['usuario']?>">Eliminar</a></td>
      </tr>
    <?php
    }
    ?>
    </tbody>
  </table>
</body>
</html>