<!DOCTYPE html>
<html>
<head>
	<title>Ver Recetas</title>
	<link rel= "stylesheet" type ="text/css" href="css/csstablas.css">
	<link href="https://fonts.googleapis.com/css?family=Pacifico|Sacramento" rel="stylesheet">
</head>
<body>
	
	
 <div id ="agrupar" class="container">
  <section id="seccion">
          
        </section>
           
        <article id="arquitectura">
          <header>
            
          </header>
          <p>
           <table id="diseño">
		<thead>
			<tr >
			<th>Id</th>
			<th>Nombre</th>
			<th>Imagen</th>
			<th>Ingredientes </th>
			<th>Procedimiento </th>
			<th>Categoria </th>
			<th colspan="2">Operaciones </th>
			<tr>  
		</thead>

		<tbody>
		<?php
			include("conexion.php");

			$query = "SELECT * FROM recetario";
			$resultado= $conexion->query($query);
			while($row = $resultado->fetch_assoc())
			{
				?>
				<tr>
				<td><?php echo $row['id']; ?></td>
				<td><?php echo $row['nombre'];?></td>
				<td><img height="70px" src="data:image/jpg;base64,<?php echo base64_encode($row['imagen']); ?>"/> </td>
				<td><?php echo $row['ingredientes']; ?></td>
				<td><?php echo $row['procedimiento']; ?></td>
				<td><?php echo $row['categoria']; ?></td>
				<td><a href="php/status.php?id=<?php echo $row['id']?>">Modificar </a></td>
				<td><a href="eliminar.php?nombre=<?php echo $row['nombre']?>&id=<?php echo $row['id']?>">Eliminar</a></td>
			</tr>
			<?php
           }
          ?>
		</tbody>
	</table>
		
	       
          </p>
        </article>
        <a href="principal.php"><button>Regresar</button></a> 
  </div>



</body>
</html>

