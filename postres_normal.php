<!DOCTYPE html>
<html>
<head>
	<title>Postres</title>
	<link rel= "stylesheet" type ="text/css" href="css/csstablas.css">
	<link href="https://fonts.googleapis.com/css?family=Pacifico|Sacramento" rel="stylesheet">
</head>

<body>
	<div>
	<table >
		<thead>
			<tr >
			<th>Id</th>
			<th>Nombre</th>
			<th>Imagen</th>
			<th>Ingredientes </th>
			<th>Procedimiento </th>
			<th>Categoria </th>
			<tr>  
		</thead>

		<tbody>
		<?php
			include("conexion.php");

			
			$query = "SELECT * FROM recetario WHERE categoria  LIKE 'postres'";
			$resultado= $conexion->query($query);
			while($row = $resultado->fetch_assoc()){
		?>
			<tr>
				<td><?php echo $row['id']; ?></td>
				<td><?php echo $row['nombre'];?></td>
				<td><img height="70px" src="data:image/jpg;base64,<?php echo base64_encode($row['imagen']); ?>"/> </td>
				<td><?php echo $row['ingredientes']; ?></td>
				<td><?php echo $row['procedimiento']; ?></td>
				<td><?php echo $row['categoria']; ?></td>

			</tr>

		<?php
           }

          ?>



		</tbody>
	
	</table>
	<a href="principal_normal.php"><button>Regresar</button></a> 
    

</div>

</body>
</html>

</body>
</html>